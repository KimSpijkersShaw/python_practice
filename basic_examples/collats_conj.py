''' Take any number and test that the colatz conjecture works on it '''
class TimeOutException(Exception):
    ' Times out if there are too many steps '

def input_number():
    ' Takes an integer input from the user '
    num = input("Please choose a number: ")
    while not num.isdigit():
        num = input("That was not an integer. /nPlease give an integer: ")
    return int(num)

def process_number(num):
    ' Apply the rules of the colatz conjecture at each step '
    if num % 2 == 0:
        return num / 2
    return num * 3 + 1

def demo_colatz(num = input_number()):
    ' Process the number at each step until it reaches 1 or times out '
    count = 0
    output = []
    while num != 1:
        count += 1
        if count > 100000:
            print("This is taking far too long!")
            raise TimeOutException
        num = process_number(num)
        output.append(f"Number is {int(num)} after {count} steps")
    print(output)
    return output

if __name__ == '__main__':
    demo_colatz()
    demo_colatz(5)
