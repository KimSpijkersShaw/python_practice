# l = [1,2,3,4,5,6,7,8]

# def is_even(n):
#     if n % 2 == 0:
#         return True
#     return False
# evens = list(filter(is_even, l))
# print(f"The even numbers are {evens}")

names = ["Kim", "Molly", "Szymon", "Cameron", "Martha", "Hassan", "Steve", "Matt"]
def contains_s(name):
    if name.lower().count("s") > 0:
        return True

def is_long(name):
    if len(name) > 5:
        return True

# s_names = list(filter(contains_s, names))
# print(f"All the names containing the letter s are: {s_names}")
# long_names = list(filter(is_long,names))
# print(f"All the long names are: {long_names}")

## From a file
f = open('word_list.txt')
lines = [word.strip() for word in f.readlines()]
s_lines = list(filter(contains_s, lines))
print(f"All the lines containing the letter s are: {s_lines}")
long_lines = list(filter(is_long, lines))
print(f"All the long lines are: {long_lines}")
