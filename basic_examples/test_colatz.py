import unittest
import collats_conj

class TestColatzDemo(unittest.TestCase):
    def test_process_num(self):
        self.assertEqual(collats_conj.process_number(3), 10)
        self.assertEqual(collats_conj.process_number(8), 4)
        self.assertEqual(collats_conj.process_number(72), 36)
        self.assertEqual(collats_conj.process_number(95), 286)
    
    def test_demo_colatz(self):
        self.assertIn('Number is 1 after', collats_conj.demo_colatz(100)[-1])
        self.assertIn('Number is 16 after 1 steps', collats_conj.demo_colatz(5))
        self.assertNotIn('Number is 2.5 after 1 steps', collats_conj.demo_colatz(5))
        self.assertIn('Number is 161 after 14 steps', collats_conj.demo_colatz(54))
        self.assertNotIn('Number is 484 after 14 steps', collats_conj.demo_colatz(54))

    def test_input_number(self):
        self.assertIsInstance(collats_conj.input_number(), int)

if __name__ == '__main__':
    unittest.main()
