class EmptyTankException(Exception):
    pass

class VehicleLockedException(Exception):
    pass

class Vehicle():
    "Vehicle class, from which others inherit"
    def __init__(self, car_name):
        self.name = car_name
        self.distance = 0
        self.speed = 0
        self.locked = True

    def set_speed(self, speed):
        self.speed = speed

    def set_distance(self, distance):
        self.distance = distance

    def unlock(self):
        self.locked = False

    def go(self):
        "Moves the car if it is unlocked"
        self.distance += self.speed
        print(f"Moved {self.speed} metres. Total distance travelled is {self.distance}")

class Car(Vehicle):
    "Car class, requires fuel to go"
    def __init__(self, car_name):
        super().__init__(self)
        self.fuel_remaining = 0
        self.tank_size = 30

    def fill_tank(self):
        "Fills the tank of the car"
        self.fuel_remaining = self.tank_size

    def go(self):
        "Moves the car if it is unlocked and has fuel"
        if self.locked:
            raise VehicleLockedException
        elif self.fuel_remaining <= 0:
            raise EmptyTankException
        self.distance += self.speed
        self.fuel_remaining -= 10

class Bike(Vehicle):
    "A simple vehicle class"
    pass