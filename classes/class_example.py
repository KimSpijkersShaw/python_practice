class EmptyTankException(Exception):
    pass

class VehicleLockedException(Exception):
    pass

class Vehicle:
    def __init__(self):
        self.distance = 0
        self.speed = 10
        self.locked = True
    
    def go(self):
        if self.locked:
            raise VehicleLockedException
        self.distance += self.speed
        print(f"Moved {self.speed} metres. Total distance travelled is {self.distance}")

    def unlock(self):
        self.locked = False

class Car(Vehicle):
    def __init__(self):
        super().__init__()
        self.speed = 20
        self.fuel_remaining = 0
        self.tank_size = 30


    def fill_tank(self):
        self.fuel_remaining = self.tank_size

    def go(self):
        if self.locked:
            raise VehicleLockedException
        elif self.fuel_remaining <= 0:
            raise EmptyTankException

        self.distance += self.speed
        self.fuel_remaining -= 10
        print(f"Moved {self.speed} metres. Total distance travelled is {self.distance}, fuel remaining is {self.fuel_remaining}")



class Bicyle(Vehicle):
    pass


if __name__ == "__main__":
    def my_code():
        try:
            car1 = Car()
        #    car1.unlock()
            car1.fill_tank()
            bike1 = Bicyle()
        #    bike1.unlock()
            car1.go()
            car1.go()
            car1.go()
            car1.go()
            bike1.go()
        except VehicleLockedException:
            print("The vehicle you want to move is locked!")
        except EmptyTankException:
            print("You have no fuel!")

        print(f"At the end, the distances travelled are {car1.distance} and {bike1.distance}")