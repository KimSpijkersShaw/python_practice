import Vehicles
vehicle_list = []
try:
    car1 = Vehicles.Car("Red")
    print(type(car1))
    car1.set_distance(10)
    car1.set_speed(10)
    vehicle_list.append(car1)
    car2 = Vehicles.Vehicle("Blue")
    car2.set_speed(12)
    vehicle_list.append(car2)
    bike1 = Vehicles.Bike("Green")
    bike1.set_speed(2)
    vehicle_list.append(bike1)
    car1.fill_tank()
    car1.unlock()
    car1.go()
except Vehicles.VehicleLockedException:
    print("The vehicle you want to move is locked!")
except Vehicles.EmptyTankException:
    print("You have no fuel!")

for i in range(8):
    print(f"after {i} time units:")
    for vehicle in enumerate(vehicle_list):
        try:
            vehicle.go()
            vehicle.give_details()
        except Vehicles.VehicleLockedException:
            print("The vehicle you want to move is locked!")
        except Vehicles.EmptyTankException:
            print("You have no fuel!")