import datetime
import json
import requests

def endpoint(event, context):
    current_time = datetime.datetime.now().time()
    r = requests.get('http://api.myip.com')
 
    body = {
        "message": "Hello, the current time is " + str(current_time),
        "ip": r.text
    }
 
    response = {
        "statusCode": 200,
        "body": json.dumps(body)
    }
 
    return response

if __name__ == '__main__':
    print(endpoint(1,1))