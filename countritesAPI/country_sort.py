import requests
import json

r = requests.get('https://restcountries.eu/rest/v2/all')
j = json.loads(r.text)
langdets = list(map(lambda x: x['languages'],j))
langs = []
for country in langdets:
    langs.extend(list(map(lambda x: x['name'], country)))

skellangs = []
counts = []
for lang in langs:
    if lang not in skellangs and lang != "Māori":
        skellangs.append(lang)
        counts.append([lang, langs.count(lang)])

counts.sort(key = lambda x: x[1], reverse = True)

output = "Language    Countries used\n"
for item in counts:
    output += '{:<22} {:<3}\n'.format(item[0],item[1])

print(output)
# print("Language    Countries used")
# for item in counts:
#     print('{:<22} {:<3}'.format(item[0],item[1]))
# print("The language spoken in the most countries is: " + str(max(counts, key=lambda x: x[1])[0]))