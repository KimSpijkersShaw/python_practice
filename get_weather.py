import requests
import json

r = requests.get('https://www.metaweather.com/api/location/31278')
data = json.loads(r.content)
windspeed = data['consolidated_weather'][0]['wind_speed']
winddir = data['consolidated_weather'][0]['wind_direction_compass']
print(f"The wind is blowing at {windspeed} miles per hour in a {winddir} direction")