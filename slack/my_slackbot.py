import json
import requests

def get_languages():
    'Gets languages from restcountries and pulls out stats from them'
    countries = requests.get('https://restcountries.eu/rest/v2/all')
    j = json.loads(countries.text)
    langdets = list(map(lambda x: x['languages'],j))
    langs = []
    for country in langdets:
        langs.extend(list(map(lambda x: x['name'], country)))

    skellangs = []
    counts = []
    for lang in langs:
        if lang not in skellangs and lang != "Māori":
            skellangs.append(lang)
            counts.append([lang, langs.count(lang)])

    counts.sort(key = lambda x: x[1], reverse = True)

    output = "Language    Countries used\n"
    for item in counts:
        output += '{:<22} {:<3}\n'.format(item[0],item[1])
    return output


URL = "https://hooks.slack.com/services/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"
MESSAGE = get_languages()
PAYLOAD = """
{
 "channel": "#academy_api_testing",
 "username": "LanguagesBot",
 "text": """
PAYLOAD += '"' + MESSAGE + '"'
PAYLOAD += """,
 "color": "warning",
 "icon_url": "https://www.searchpng.com/wp-content/uploads/2019/02/Avengers-Logo-PNG-Transparent-Avengers-Logo-715x715.png"
}"""
r = requests.post(URL, data=PAYLOAD)
print(r.status_code)
