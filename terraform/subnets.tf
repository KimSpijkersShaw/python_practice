resource "aws_subnet" "pub1" {
  vpc_id                  = aws_vpc.vpc.id
  cidr_block              = "172.31.0.0/17"
  map_public_ip_on_launch = true
  tags = {
    Name = "Kims VPC subnet 1"
  }
}

resource "aws_route_table" "pub1" {
  vpc_id = aws_vpc.vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.default.id
  }
  tags = {
    Name = "Kims Pub1 Route Table"
  }
}

resource "aws_route_table_association" "pub1" {
  subnet_id      = aws_subnet.pub1.id
  route_table_id = aws_route_table.pub1.id
}

output "subnet_id" {
  value       = aws_subnet.pub1.id
  description = "VPC Subnet 1"
}