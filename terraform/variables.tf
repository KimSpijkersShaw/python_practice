variable "vpc_cidr" {
  type    = string
  default = "172.31.0.0/16"
}

variable "tags" {
  default = {
    Name       = "Kim demo"
    Start_date = "2020-11-10"
  }
}

variable "homeip" {
  type    = string
  default = "92.12.96.128/32"
}

variable "image_id" {
  default = {
    eu-west-1 = "ami-0bb3fad3c0286ebd5"
    us-west-2 = "ami-0528a5175983e7f28"
  }
}

variable "region" {
  type    = string
  default = "us-west-2"
}

variable "instance_type" {
  type    = string
  default = "t3a.micro"
}

variable "keyname" {
  type    = string
  default = "kimOregon"
}
