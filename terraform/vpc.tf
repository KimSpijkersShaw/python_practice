resource "aws_vpc" "vpc" {
  cidr_block           = var.vpc_cidr
  enable_dns_support   = true
  enable_dns_hostnames = true
  tags                 = var.tags
}

resource "aws_internet_gateway" "default" {
  vpc_id = aws_vpc.vpc.id
}

output "vpc_id" {
  value       = aws_vpc.vpc.id
  description = "The VPC ID created"
}

output "vpc_sg_id" {
  value       = aws_vpc.vpc.default_security_group_id
  description = "The VPC Security Group that was created"
}

output "vpc_default_route_table" {
  value       = aws_vpc.vpc.default_route_table_id
  description = "The default route table for VPC"
}

output "aws_internet_gateway" {
  value       = aws_internet_gateway.default.id
  description = "Internet Gateway ID"
}