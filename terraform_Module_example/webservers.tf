resource "aws_instance" "web1" {
  ami                    = var.image_id[var.region]
  instance_type          = var.instance_type
  key_name               = var.keyname
  vpc_security_group_ids = [aws_security_group.web.id]
  subnet_id              = module.vpc.public_subnets[0]
  user_data              = <<EOF
    #!/bin/bash
    yum -y install httpd
    systemctl enable httpd
    systemctl start httpd
    echo "<h1>Welcome to Kim's web server</h1>" >/var/www/html/index.html
EOF

  tags = {
    Name = "Kims Web Server"
  }
}

output "web_pub_dns" {
  value       = aws_instance.web1.public_dns
  description = "Webserver DNS name"
}

output "web_pub_id" {
  value       = aws_instance.web1.id
  description = "Webserver instance ID"
}