def shortnames(threshold=3):
    f = open('animals.txt')
    ans = map(lambda a: a.strip(), f.readlines())
    f.close()
    shorts = list(filter(lambda x: len(x) <= threshold, ans))
    shorts.sort()
    return shorts

def contains_letter(char):
    f = open('animals.txt')
    ans = list(map(lambda a: a.strip(), f.readlines()))
    f.close()
    ans_with = list(filter(lambda x: x.count(char) > 0, ans))
    return ans_with
    
