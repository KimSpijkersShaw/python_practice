""" simple maths library """

def double(num):
    " multiple a number by two "
    return num

def triple(num):
    " multiple a number by three "
    return num ** 3
