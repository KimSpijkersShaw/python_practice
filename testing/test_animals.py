import unittest
import animals

class Animals(unittest.TestCase):
    def test_shortnames(self):
        self.assertEqual(animals.shortnames(), ['cat', 'dog'])
        self.assertEqual(animals.shortnames(4), ['cat', 'dog', 'fish'])

    def test_contains_letter_a(self):
        self.assertEqual(animals.contains_letter('a'), ['cat', 'giraffe', 'elephant'])
        self.assertIn('cat', animals.contains_letter('a'))
        self.assertNotIn('dog', animals.contains_letter('a'))

if __name__ == '__main__':
    unittest.main()
