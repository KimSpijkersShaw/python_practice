''' Test script for maths.py '''
import unittest
import maths

class MathsTest(unittest.TestCase):
    ' tests '
    def test_double(self):
        ' check that double function works '
        self.assertEqual(maths.double(10), 20)
        self.assertEqual(maths.double(3.5), 7)
        self.assertEqual(maths.double(-10), -20)

    def test_triple(self):
        ' check that triple function gives desired results '
        self.assertEqual(maths.triple(10), 30)

if __name__ == '__main__':
    unittest.main()
